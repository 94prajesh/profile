import { Component, HostListener } from '@angular/core';
import * as $ from "jquery";

const pageId = [
  'home',
  'services',
  'career',
  'contactus'
]

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'profile';
  currentPage = 'home'

  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    this.onScroll()
  }

  onScroll() {
    const isElementInView = [
      { visible: true },
      { visible: this.isElementInView($('#' + pageId[1]), false) },
      { visible: this.isElementInView($('#' + pageId[2]), false) },
      { visible: this.isElementInView($('#' + pageId[3]), false) }
    ];

    const reverseElement = isElementInView.reverse();

    const index = reverseElement.findIndex(each => each.visible)

    const calculatedPage = pageId[reverseElement.length - index - 1]

    this.currentPage = calculatedPage
  }

  isElementInView(element, fullyInView): boolean {
    var pageTop = $(window).scrollTop();
    var pageBottom = pageTop + $(window).height();
    var elementTop = $(element).offset().top + ($(element).height() / 2);
    var elementBottom = elementTop + $(element).height();

    if (fullyInView === true) {
      return ((pageTop < elementTop));
    } else {
      return ((elementTop - this.getVhtoPx(10) <= pageBottom) && (elementBottom >= pageTop));
    }
  }

  showHideMenu() {
    var element = document.getElementById('nav-menu')
    if(element.className==='') {
      element.className += 'responsive'
    } else {
      element.className=''
    }
  }

  getVhtoPx(vh: number) {
    const unitVh = window.innerHeight / 100
    return vh * unitVh
  }
}
