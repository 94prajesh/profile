export interface IContactUsView {

    showLoader(isShow: boolean);

    onSuccess();

    onError();

    sendMessage(event: Event);
}