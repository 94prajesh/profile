export class Email {
    public name;
    public email;
    public number;
    public company;
    public message;

    constructor (message: any) {
        this.name = message.name;
        this.email = message.email;
        this.number = message.number;
        this.company = message.company;
        this.message = message.message;
    }
}