import { FormGroup } from '@angular/forms';

import { IContactUsPresenter } from './interface.contact-us.presenter';
import { Email } from '../entity/email';
import { mark } from '../../../common/utils/forms/mark-as-touched';
import { IContactUsView } from '../view/interface.contact-us.view';
import { IContactUsInteractor } from '../interactor/interface.contact-us.interactor';

export class ContactUsPresenter implements IContactUsPresenter {
    private view: IContactUsView;
    private interactor: IContactUsInteractor;

    constructor(view: IContactUsView, interactor: IContactUsInteractor) {
        this.view = view;
        this.interactor = interactor;
    }

    validateForm(form: FormGroup) {
        mark(form);

        return form.valid;
    }    
    
    postMessage(form:FormGroup) {

        var result = this.validateForm(form);

        if (!result) {
            this.view.showLoader(false);
            return
        };

        this.view.showLoader(true);

        const email: Email = new Email(form.value);

        this.interactor.sendEmail(email).subscribe(
            success => {
                this.view.onSuccess();
            },
            error => {
                this.view.onError();
                this.view.showLoader(false);
            },
            () => {
                this.view.showLoader(false);
            }
        );
        
    }

    


}