import { FormGroup } from '@angular/forms';
import { Email } from '../entity/email';

export interface IContactUsPresenter {

    validateForm(form: FormGroup);

    postMessage(form: FormGroup);
}