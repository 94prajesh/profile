import { Component, OnInit } from '@angular/core';
import { IContactUsView } from './view/interface.contact-us.view';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IContactUsPresenter } from './presenter/interface.contact-us.presenter';
import { ContactUsPresenter } from './presenter/contact-us.presenter';
import { ContactServiceService } from './interactor/contact-service.service';

@Component({
  selector: 'contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit, IContactUsView {

  public state = {
    error: false,
    success: false,
    isSending: false
  }

  public emailForm: FormGroup;

  private presenter: IContactUsPresenter;

  constructor(
    private interactor: ContactServiceService
  ) { 
    this.presenter = new ContactUsPresenter(this, this.interactor);
  }

  ngOnInit() {
    this.setState();

    this.setEmail();
  }

  private setState() {
    this.state.error = false;
    this.state.success = false;
    this.state.isSending = false;
  }

  private setEmail() {
    this.emailForm = new FormGroup(
      {
        name: new FormControl('', [Validators.required]),
        email: new FormControl('', [Validators.email, Validators.required]),
        number: new FormControl('', []),
        company: new FormControl(''),
        message: new FormControl('', [Validators.required, Validators.maxLength(2000)])
      }
    )
  }

  showLoader(isShow: boolean) {
    this.state.isSending = isShow;

    return isShow ? this.state.isSending = true : this.state.isSending = false;
  }

  onSuccess() {
    this.state.success = true;
    this.emailForm.reset();

    setTimeout(() => {
      this.state.success = false;
    }, 5000);
  }

  onError() {
    this.state.error = true;

    setTimeout(() => {
      this.state.error = false;
    }, 5000);
  }

  sendMessage(event?: Event) {
    if (event) event.preventDefault();

    this.presenter.postMessage(this.emailForm);

  }

  get name(): FormControl {
    return this.emailForm.get('name') as FormControl;
  }

  get email(): FormControl {
    return this.emailForm.get('email') as FormControl;
  }

  get number(): FormControl {
    return this.emailForm.get('number') as FormControl;
  }

  get company(): FormControl {
    return this.emailForm.get('company') as FormControl;
  }

  get message(): FormControl {
    return this.emailForm.get('message') as FormControl;
  }

}
