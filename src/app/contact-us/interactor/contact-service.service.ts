import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { IContactUsInteractor } from './interface.contact-us.interactor';
import { Email } from '../entity/email';
import { Observable } from 'rxjs';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ContactServiceService implements IContactUsInteractor {

  constructor(
    private http: HttpClient
  ) { }

  sendEmail(email: Email): Observable<any> {
    let header = new HttpHeaders({
      'Access-Control-Allow-Origin': '*'
    });

    return this.http.post(
      apiUrl+'/email/send',
      email,
      { headers: header }
    );
  }

}
