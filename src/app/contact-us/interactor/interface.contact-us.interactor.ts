import { Email } from '../entity/email';
import { Observable } from 'rxjs';

export interface IContactUsInteractor {

    sendEmail(email: Email): Observable<any>;
    
}