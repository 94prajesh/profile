import { FormGroup } from '@angular/forms';

export function mark(form: FormGroup) {
    (<any>Object).values(form.controls).forEach(control => {
        control.markAsTouched();

        if (control.controls) {
            this.markFormGroupTouched(control);
        }
    });
}